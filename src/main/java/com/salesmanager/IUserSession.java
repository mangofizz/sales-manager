package com.salesmanager;

import com.salesmanager.data.StoreUser;

public interface IUserSession {
    public StoreUser getUser();
    public void destroy();
}
