package com.salesmanager;

import com.salesmanager.data.StoreUser;
import com.salesmanager.data.StoreUserRepository;

public class Authenticator {
    public enum StoreUserAuthResult {
        SUCCESS,
        INVALID_CREDENTIALS,
        INCORRECT_PASSWORD,
        DATABASE_ERROR
    }

    public enum StoreUserRegisterResult {
        SUCCESS,
        USER_ALREADY_EXISTS,
        DATABASE_ERROR
    }

    public enum StoreUserUnregisterResult {
        SUCCESS,
        USER_DOES_NOT_EXIST,
        DATABASE_ERROR
    }

    private StoreUserRepository storeUserRepository;

    public Authenticator() {
        storeUserRepository = new StoreUserRepository();
    }

    public StoreUserAuthResult authenticate(String username, String password) {
        StoreUser storeUser = storeUserRepository.getByUsername(username);

        if(storeUser == null) {
            return StoreUserAuthResult.INVALID_CREDENTIALS;
        }

        if(storeUser.getPassword().equals(password)) {
            return StoreUserAuthResult.SUCCESS;
        }

        return StoreUserAuthResult.INCORRECT_PASSWORD;
    }

    public StoreUserRegisterResult register(String username, String password, String name, String firstSurname, String secondSurname) {
        StoreUser storeUser = storeUserRepository.getByUsername(username);

        if(storeUser != null) {
            return StoreUserRegisterResult.USER_ALREADY_EXISTS;
        }

        storeUser = new StoreUser();
        storeUser.setUsername(username);
        storeUser.setPassword(password);
        storeUser.setName(name);
        storeUser.setFirstSurname(firstSurname);
        storeUser.setSecondSurname(secondSurname);

        try {
            storeUserRepository.create(storeUser);
        }
        catch (Exception e) {
            return StoreUserRegisterResult.DATABASE_ERROR;
        }

        return StoreUserRegisterResult.SUCCESS;
    }

    public StoreUserUnregisterResult unregister(String username) {
        StoreUser storeUser = storeUserRepository.getByUsername(username);

        if(storeUser == null) {
            return StoreUserUnregisterResult.USER_DOES_NOT_EXIST;
        }

        try {
            storeUserRepository.delete(storeUser);
        }
        catch (Exception e) {
            return StoreUserUnregisterResult.DATABASE_ERROR;
        }

        return StoreUserUnregisterResult.SUCCESS;
    }
}
