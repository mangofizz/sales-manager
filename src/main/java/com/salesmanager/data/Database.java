package com.salesmanager.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.salesmanager.Config;
import com.salesmanager.Utils;

import javafx.scene.control.Alert.AlertType;

public class Database {    
    private Connection connection = null;
    
    public Database() {
        try {
            Config appConfigs = new Config();
            
            final String HOSTNAME  = appConfigs.getConfig("DB_HOSTNAME");
            final String DATABASE = appConfigs.getConfig("DB_NAME");
            final String USERNAME = appConfigs.getConfig("DB_USER");
            final String PASSWORD = appConfigs.getConfig("DB_PASSWORD");
            final String URL_CONEXION = "jdbc:mysql://"+HOSTNAME+"/"+DATABASE+"?serverTimezone=UTC";
            
            this.connection = DriverManager.getConnection(URL_CONEXION, USERNAME, PASSWORD);
        }
        catch (Exception e) {
            Utils.showAlert("Error de conexión", "No se ha podido establecer conexión con la base de datos", AlertType.ERROR);
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() {
        try {
            this.connection.close();
        } 
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        return this.connection;
    }
}

