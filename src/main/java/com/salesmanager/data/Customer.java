package com.salesmanager.data;

public class Customer {
    public int id;
    public String name;
    public String firstSurname;
    public String secondSurname;
    public String rfc;
    private String email;
    private String phone;

    public Customer() {

    }

    public Customer(int id, String name, String firstSurname, String secondSurname, String rfc, String email, String phone) {
        this.id = id;
        this.name = name;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.rfc = rfc;
        this.email = email;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getFullName() {
        return name + " " + firstSurname + " " + secondSurname;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean equals(Customer customer) {
        return this.id == customer.id;
    }

    @Override
    public String toString() {
        return this.getFullName() + " (" + this.rfc + ")";
    }
}
