package com.salesmanager.data;

public class ProductSold {
    private int id;
    private Product product;
    private Sale sale;
    private int amount;

    public ProductSold() {

    }

    public ProductSold(int id, Product product, Sale sale, int quantity) {
        this.id = id;
        this.product = product;
        this.sale = sale;
        this.amount = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public float getTotal() {
        return this.product.getPrice() * this.amount;
    }

    public boolean equals(ProductSold productSold) {
        return this.id == productSold.id;
    }

    @Override
    public String toString() {
        return this.product.getName() + " x" + this.amount;
    }
}
