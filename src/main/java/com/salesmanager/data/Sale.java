package com.salesmanager.data;

import java.time.LocalDate;

public class Sale {
    private int id;
    private LocalDate date;
    private int number;
    private ProductSold[] productsSold;
    private StoreUser storeUser;
    private Customer customer;

    public Sale() {

    }

    public Sale(int id, LocalDate date, int number, ProductSold[] productsSold, StoreUser storeUser, Customer customer) {
        this.id = id;
        this.date = date;
        this.number = number;
        this.productsSold = productsSold;
        this.storeUser = storeUser;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ProductSold[] getProductsSold() {
        return productsSold;
    }

    public void setProductsSold(ProductSold[] productsSold) {
        this.productsSold = productsSold;
    }

    public StoreUser getStoreUser() {
        return storeUser;
    }

    public void setStoreUser(StoreUser storeUser) {
        this.storeUser = storeUser;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public float getTotal() {
        float total = 0;
        for (ProductSold productSold : productsSold) {
            total += productSold.getTotal();
        }
        return total;
    }

    public boolean equals(Sale sale) {
        return this.id == sale.id;
    }

    @Override
    public String toString() {
        return this.number + " " + this.customer.toString() + " (" + this.date + ")";
    }
}
