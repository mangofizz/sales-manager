package com.salesmanager.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductRepository implements IRepository<Product> {
    private Database database;

    public ProductRepository() {
        this.database = new Database();
    }

    public ProductRepository(Database database) {
        this.database = database;
    }

    @Override
    public void create(Product product) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "INSERT INTO product (code, name, cost, price, stock) VALUES (?, ?, ?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, product.getCode());
                statement.setString(2, product.getName());
                statement.setFloat(3, product.getCost());
                statement.setFloat(4, product.getPrice());
                statement.setInt(5, product.getStock());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al insertar el producto a la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public void update(Product product) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "UPDATE product SET code = ?, name = ?, cost = ?, price = ?, stock = ? WHERE productID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, product.getCode());
                statement.setString(2, product.getName());
                statement.setFloat(3, product.getCost());
                statement.setFloat(4, product.getPrice());
                statement.setInt(5, product.getStock());
                statement.setInt(6, product.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al actualizar el producto en la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public void delete(Product product) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "DELETE FROM product WHERE productID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, product.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al eliminar el producto de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public Product get(int id) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM product WHERE productID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, id);
                ResultSet resultSet = statement.executeQuery();

                if(resultSet.next()) {
                    Product product = new Product();
                    product.setId(resultSet.getInt("productID"));
                    product.setCode(resultSet.getString("code"));
                    product.setName(resultSet.getString("name"));
                    product.setCost(resultSet.getFloat("cost"));
                    product.setPrice(resultSet.getFloat("price"));
                    product.setStock(resultSet.getInt("stock"));
                    return product;
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al obtener el producto de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }

    @Override
    public ArrayList<Product> getAll() {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM product";
                PreparedStatement statement = connection.prepareStatement(query);
                ResultSet resultSet = statement.executeQuery();

                ArrayList<Product> products = new ArrayList<Product>();
                while(resultSet.next()) {
                    Product product = new Product();
                    product.setId(resultSet.getInt("productID"));
                    product.setCode(resultSet.getString("code"));
                    product.setName(resultSet.getString("name"));
                    product.setCost(resultSet.getFloat("cost"));
                    product.setPrice(resultSet.getFloat("price"));
                    product.setStock(resultSet.getInt("stock"));
                    products.add(product);
                }
                return products;
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al obtener los productos de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }

    public boolean exists(String code) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM product WHERE code = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, code);
                ResultSet resultSet = statement.executeQuery();

                if(resultSet.next()) {
                    return true;
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al obtener el producto de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
        return false;
    }
}
