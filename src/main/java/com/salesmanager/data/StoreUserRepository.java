package com.salesmanager.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StoreUserRepository implements IRepository<StoreUser> {
    private Database database;

    public StoreUserRepository() {
        this.database = new Database();
    }

    public StoreUserRepository(Database database) {
        this.database = database;
    }

    public StoreUser getByUsername(String username) {
        StoreUser storeUser = null;
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM storeuser WHERE username = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, username);
                ResultSet queryResult = statement.executeQuery();

                if(queryResult.next()) {
                    storeUser = new StoreUser();
                    storeUser.setId(queryResult.getInt("storeUserID"));
                    storeUser.setName(queryResult.getString("name"));
                    storeUser.setFirstSurname(queryResult.getString("firstSurname"));
                    storeUser.setSecondSurname(queryResult.getString("secondSurname"));                    
                    storeUser.setUsername(queryResult.getString("username"));
                    storeUser.setPassword(queryResult.getString("password"));
                    storeUser.setType(StoreUser.UserType.values()[queryResult.getInt("type")]);
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al obtener el usuario de la tienda");
            System.err.println("Excepción: " + e.getMessage());
        }
        return storeUser;
    }

    @Override
    public void create(StoreUser user) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "INSERT INTO storeuser (name, firstSurname, secondSurname, username, password, type) VALUES (?, ?, ?, ?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, user.getName());
                statement.setString(2, user.getFirstSurname());
                statement.setString(3, user.getSecondSurname());
                statement.setString(4, user.getUsername());
                statement.setString(5, user.getPassword());
                statement.setInt(6, user.getType().getValue());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al insertar el usuario a la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public void update(StoreUser user) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "UPDATE storeuser SET name = ?, firstSurname = ?, secondSurname = ?, username = ?, type = ? WHERE storeUserID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, user.getName());
                statement.setString(2, user.getFirstSurname());
                statement.setString(3, user.getSecondSurname());
                statement.setString(4, user.getUsername());
                statement.setInt(5, user.getType().getValue());
                statement.setInt(6, user.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al actualizar el usuario en la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public void delete(StoreUser storeUser) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "DELETE FROM storeuser WHERE storeUserID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, storeUser.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al eliminar el usuario de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public StoreUser get(int id) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM storeuser WHERE storeUserID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, id);
                ResultSet queryResult = statement.executeQuery();
                
                if(queryResult.next()) {
                    StoreUser aux = new StoreUser();
                    aux.setId(queryResult.getInt("storeUserID"));
                    aux.setName(queryResult.getString("name"));
                    aux.setFirstSurname(queryResult.getString("firstSurname"));
                    aux.setSecondSurname(queryResult.getString("secondSurname"));                    
                    aux.setUsername(queryResult.getString("username"));
                    aux.setPassword(queryResult.getString("password"));
                    aux.setType(StoreUser.UserType.values()[queryResult.getInt("type")]);
                    return aux;
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al recuperar la información del usuario");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }

    @Override
    public ArrayList<StoreUser> getAll() {
        try {
            Connection connection = database.getConnection();

            ArrayList<StoreUser> users = new ArrayList<>();
            if(connection != null) {
                String query = "SELECT * FROM storeuser";
                PreparedStatement statement = connection.prepareStatement(query);
                ResultSet queryResult = statement.executeQuery();
                
                while(queryResult.next()) {
                    StoreUser aux = new StoreUser();
                    aux.setId(queryResult.getInt("storeUserID"));
                    aux.setName(queryResult.getString("name"));
                    aux.setFirstSurname(queryResult.getString("firstSurname"));
                    aux.setSecondSurname(queryResult.getString("secondSurname"));                    
                    aux.setUsername(queryResult.getString("username"));
                    aux.setType(StoreUser.UserType.values()[queryResult.getInt("type")]);
                    users.add(aux);
                }
                return users;
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al recuperar la información de los usuarios");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }

    // Update the password of the user
    public void updatePassword(StoreUser user, String newPasword) {
        try {
            Connection connection = database.getConnection();
            if(connection != null) {
                String query = "UPDATE storeuser SET password = ? WHERE storeUserID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, newPasword);
                statement.setInt(2, user.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al actualizar la contraseña del usuario");
            System.err.println("Excepción: " + e.getMessage());
        }
    }
}
