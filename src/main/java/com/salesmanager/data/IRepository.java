package com.salesmanager.data;

import java.util.ArrayList;

public interface IRepository<T> {
    public void create(T object);
    public void update(T object);
    public void delete(T object);
    public T get(int id);
    public ArrayList<T> getAll();
}
