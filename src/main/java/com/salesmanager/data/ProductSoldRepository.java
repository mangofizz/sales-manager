package com.salesmanager.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ProductSoldRepository implements IRepository<ProductSold> {
    private Database database;

    public ProductSoldRepository() {
        this.database = new Database();
    }

    public ProductSoldRepository(Database database) {
        this.database = database;
    }

    public void create(ProductSold productSold) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "INSERT INTO productsold (saleID, productID, amount) VALUES (?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, productSold.getSale().getId());
                statement.setInt(2, productSold.getProduct().getId());
                statement.setInt(3, productSold.getAmount());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al insertar el producto vendido a la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    public void update(ProductSold productSold) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "UPDATE productsold SET saleID = ?, productID = ?, amount = ? WHERE productSoldID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, productSold.getSale().getId());
                statement.setInt(2, productSold.getProduct().getId());
                statement.setInt(3, productSold.getAmount());
                statement.setInt(4, productSold.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al actualizar el producto vendido en la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    public void delete(ProductSold productSold) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "DELETE FROM productsold WHERE productSoldID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, productSold.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al eliminar el producto vendido de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    public ProductSold get(int id) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM productsold WHERE productSoldID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, id);
                ResultSet resultSet = statement.executeQuery();

                if(resultSet.next()) {
                    SaleRepository saleRepository = new SaleRepository(this.database);
                    ProductRepository productRepository = new ProductRepository(this.database);

                    ProductSold productSold = new ProductSold();
                    productSold.setId(resultSet.getInt("productSoldID"));
                    productSold.setAmount(resultSet.getInt("amount"));
                    productSold.setSale(saleRepository.get(resultSet.getInt("saleID")));
                    productSold.setProduct(productRepository.get(resultSet.getInt("productID")));
                    return productSold;
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al obtener el producto vendido de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }

    public ArrayList<ProductSold> getAll() {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM productsold";
                PreparedStatement statement = connection.prepareStatement(query);
                ResultSet resultSet = statement.executeQuery();

                ArrayList<ProductSold> productsSold = new ArrayList<ProductSold>();
                SaleRepository saleRepository = new SaleRepository(this.database);
                ProductRepository productRepository = new ProductRepository(this.database);

                while(resultSet.next()) {
                    ProductSold productSold = new ProductSold();
                    productSold.setId(resultSet.getInt("productSoldID"));
                    productSold.setAmount(resultSet.getInt("amount"));
                    productSold.setSale(saleRepository.get(resultSet.getInt("saleID")));
                    productSold.setProduct(productRepository.get(resultSet.getInt("productID")));
                    productsSold.add(productSold);
                }
                return productsSold;
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al obtener los productos vendidos de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }
}
