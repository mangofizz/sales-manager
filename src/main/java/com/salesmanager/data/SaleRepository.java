package com.salesmanager.data;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class SaleRepository implements IRepository<Sale> {
    private Database database;

    public SaleRepository() {
        this.database = new Database();
    }

    public SaleRepository(Database database) {
        this.database = database;
    }

    public void create(Sale sale) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "INSERT INTO sale (saleDate, saleNumber, totalSale, customerID, storeUserID) VALUES (?, ?, ?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                statement.setDate(1, Date.valueOf(sale.getDate()));
                statement.setInt(2, sale.getNumber());
                statement.setFloat(3, sale.getTotal());

                var customer = sale.getCustomer();
                if(customer != null) {
                    statement.setInt(4, customer.getId());
                }
                else {
                    statement.setNull(4, java.sql.Types.INTEGER);
                }
                
                statement.setInt(5, sale.getStoreUser().getId());
                statement.executeUpdate();

                ResultSet generatedKeys = statement.getGeneratedKeys();
                if(generatedKeys.next()) {
                    sale.setId(generatedKeys.getInt(1));
                }

                ProductSoldRepository productSoldRepository = new ProductSoldRepository(database);
                ProductRepository productRepository = new ProductRepository(database);
                for(ProductSold productSold : sale.getProductsSold()) {
                    var product = productSold.getProduct();
                    product.setStock(product.getStock() - productSold.getAmount());
                    productRepository.update(product);

                    productSold.setSale(sale);
                    productSoldRepository.create(productSold);
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al insertar la venta a la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    public void update(Sale sale) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "UPDATE sale SET saleDate = ?, saleNumber = ?, totalSale = ?, customerID = ?, storeUserID = ? WHERE saleID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setDate(1, Date.valueOf(sale.getDate()));
                statement.setInt(2, sale.getNumber());
                statement.setFloat(3, sale.getTotal());
                statement.setInt(4, sale.getCustomer().getId());
                statement.setInt(5, sale.getStoreUser().getId());
                statement.setInt(6, sale.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al actualizar la venta en la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    public void delete(Sale sale) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "DELETE FROM sale WHERE saleID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, sale.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al eliminar la venta de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    public Sale get(int id) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM sale WHERE saleID = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, id);
                ResultSet resultSet = statement.executeQuery();

                if(resultSet.next()) {
                    CustomerRepository customerRepository = new CustomerRepository(this.database);
                    StoreUserRepository storeUserRepository = new StoreUserRepository(this.database);
                    Sale sale = new Sale();
                    sale.setId(resultSet.getInt("saleID"));
                    sale.setDate(resultSet.getDate("saleDate").toLocalDate());
                    sale.setNumber(resultSet.getInt("saleNumber"));
                    sale.setCustomer(customerRepository.get(resultSet.getInt("customerID")));
                    sale.setStoreUser(storeUserRepository.get(resultSet.getInt("storeUserID")));
                    return sale;
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al obtener la venta de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }

    public ArrayList<Sale> getAll() {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM sale";
                PreparedStatement statement = connection.prepareStatement(query);
                ResultSet resultSet = statement.executeQuery();
                
                ArrayList<Sale> sales = new ArrayList<Sale>();
                CustomerRepository customerRepository = new CustomerRepository(this.database);
                StoreUserRepository storeUserRepository = new StoreUserRepository(this.database);

                while(resultSet.next()) {
                    Sale sale = new Sale();
                    sale.setId(resultSet.getInt("saleID"));
                    sale.setDate(resultSet.getDate("saleDate").toLocalDate());
                    sale.setNumber(resultSet.getInt("saleNumber"));
                    sale.setCustomer(customerRepository.get(resultSet.getInt("customerID")));
                    sale.setStoreUser(storeUserRepository.get(resultSet.getInt("storeUserID")));
                    sales.add(sale);
                }
                return sales;
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al obtener las ventas de la base de datos");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }
}
