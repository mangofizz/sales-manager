package com.salesmanager.data;

public class StoreUser {
    public enum UserType {
        ADMINISTRATOR(0),
        EMPLOYEE(1);
        
        private int value;
        
        private UserType(int value) {
            this.value = value;
        }
        
        public int getValue() {
            return this.value;
        }
        
        public boolean equals(UserType code) {
            return this.value == code.value;
        }
    }

    private int id;
    private String name;
    private String firstSurname;
    private String secondSurname;
    private String username;
    private String password;
    private UserType type;

    public StoreUser() {

    }

    public StoreUser(int id, String name, String firstSurname, String secondSurname, String username, UserType type) {
        this.id = id;
        this.name = name;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.username = username;
        this.type = type;
    }

    public StoreUser(int id, String name, String firstSurname, String secondSurname, String username, String password, UserType type) {
        this.id = id;
        this.name = name;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstSurname() {
        return firstSurname;
    }

    public void setFirstSurname(String firstSurname) {
        this.firstSurname = firstSurname;
    }

    public String getSecondSurname() {
        return secondSurname;
    }

    public void setSecondSurname(String secondSurname) {
        this.secondSurname = secondSurname;
    }

    public String getFullName() {
        return name + " " + firstSurname + " " + secondSurname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public boolean equals(StoreUser storeUser) {
        return this.id == storeUser.id;
    }

    @Override
    public String toString() {
        return this.name + " " + this.firstSurname + " " + this.secondSurname + " (" + this.username + ")";
    }
}
