package com.salesmanager.data;

public class Product {
    private int id;
    private String code;
    private String name;
    private float cost;
    private float price;
    private int stock;

    public Product() {

    }

    public Product(int id, String code, String name, float cost, float price, int stock) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.cost = cost;
        this.price = price;
        this.stock = stock;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public boolean equals(Product product) {
        return this.id == product.id;
    }

    @Override
    public String toString() {
        return this.name + " - " + this.code;
    }
}
