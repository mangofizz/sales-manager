package com.salesmanager.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CustomerRepository implements IRepository<Customer> {
    private Database database;

    public CustomerRepository() {
        database = new Database();
    }

    public CustomerRepository(Database database) {
        this.database = database;
    }

    @Override
    public void create(Customer customer) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "INSERT INTO customer (name, firstSurname, secondSurname, RFC, phone, email) VALUES (?, ?, ?, ?, ?, ?)";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, customer.getName());
                statement.setString(2, customer.getFirstSurname());
                statement.setString(3, customer.getSecondSurname());
                statement.setString(4, customer.getRfc());
                statement.setString(5, customer.getPhone());
                statement.setString(6, customer.getEmail());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al agregar el cliente");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public void update(Customer customer) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "UPDATE customer SET name = ?, firstSurname = ?, secondSurname = ?, RFC = ?, phone = ?, email = ? WHERE customerId = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, customer.getName());
                statement.setString(2, customer.getFirstSurname());
                statement.setString(3, customer.getSecondSurname());
                statement.setString(4, customer.getRfc());
                statement.setString(5, customer.getPhone());
                statement.setString(6, customer.getEmail());
                statement.setInt(7, customer.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al actualizar el cliente");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public void delete(Customer customer) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "DELETE FROM customer WHERE customerId = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, customer.getId());
                statement.executeUpdate();
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al eliminar el cliente");
            System.err.println("Excepción: " + e.getMessage());
        }
    }

    @Override
    public Customer get(int id) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM customer WHERE customerId = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setInt(1, id);
                ResultSet queryResult = statement.executeQuery();
                
                if(queryResult.next()) {
                    Customer aux = new Customer();
                    aux.setId(queryResult.getInt("customerId"));
                    aux.setName(queryResult.getString("name"));
                    aux.setFirstSurname(queryResult.getString("firstSurname"));
                    aux.setSecondSurname(queryResult.getString("secondSurname"));                    
                    aux.setRfc(queryResult.getString("RFC"));
                    aux.setPhone(queryResult.getString("phone"));
                    aux.setEmail(queryResult.getString("email"));
                    return aux;
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al recuperar la información del cliente");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }

    @Override
    public ArrayList<Customer> getAll() {
        try {
            Connection connection = database.getConnection();

            ArrayList<Customer> customers = new ArrayList<>();
            if(connection != null) {
                String query = "SELECT * FROM customer";
                PreparedStatement statement = connection.prepareStatement(query);
                ResultSet queryResult = statement.executeQuery();
                
                while(queryResult.next()) {
                    Customer aux = new Customer();
                    aux.setId(queryResult.getInt("customerId"));
                    aux.setName(queryResult.getString("name"));
                    aux.setFirstSurname(queryResult.getString("firstSurname"));
                    aux.setSecondSurname(queryResult.getString("secondSurname"));                    
                    aux.setRfc(queryResult.getString("RFC"));
                    aux.setPhone(queryResult.getString("phone"));
                    aux.setEmail(queryResult.getString("email"));
                    customers.add(aux);
                }
                return customers;
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al recuperar la información de los clientes");
            System.err.println("Excepción: " + e.getMessage());
        }
        return null;
    }

    public boolean exists(String rfc) {
        try {
            Connection connection = database.getConnection();

            if(connection != null) {
                String query = "SELECT * FROM customer WHERE RFC = ?";
                PreparedStatement statement = connection.prepareStatement(query);
                statement.setString(1, rfc);
                ResultSet queryResult = statement.executeQuery();
                
                if(queryResult.next()) {
                    return true;
                }
            }
        }
        catch (SQLException e) {
            System.err.println("Ocurrió un error al recuperar la información del cliente");
            System.err.println("Excepción: " + e.getMessage());
        }
        return false;
    }
}
