package com.salesmanager;

import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class Utils {
    public static Optional<ButtonType> showAlert(String title, String message, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        return alert.showAndWait();
    }

    public static String toFirstWordLetterUppercaseString(String str) {
        // Set first letter to uppercase on every string word
        String[] words = str.split(" ");
        String result = "";
        for(String word : words) {
            result += word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase() + " ";
        }
        return result.trim();
    }
}
