package com.salesmanager.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.control.Label;

import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.salesmanager.Authenticator;
import com.salesmanager.data.StoreUserRepository;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class AdministrationUserEditController implements Initializable {
    
    @FXML
    private JFXTextField nameTextField;
    @FXML
    private JFXTextField firstSurnameTextField;
    @FXML
    private JFXTextField secondSurnameTextField;
    @FXML
    private JFXTextField usernamTextField;
    @FXML
    private JFXPasswordField passwordTextField;
    @FXML
    private JFXPasswordField confirmPasswordField;
    @FXML
    private JFXPasswordField currentPasswordField;
    @FXML
    private Label messageLabel;

    private void loadUserData() {
        var user = DashboardController.userSession.getUser();
        nameTextField.setText(user.getName());
        firstSurnameTextField.setText(user.getFirstSurname());
        secondSurnameTextField.setText(user.getSecondSurname());
        usernamTextField.setText(user.getUsername());
    }

    @FXML
    private void saveButtonAction() {
        messageLabel.setVisible(false);

        var user = DashboardController.userSession.getUser();
        
        // Get user inputs
        var name = nameTextField.getText();
        var firstSurname = firstSurnameTextField.getText();
        var secondSurname = secondSurnameTextField.getText();
        var username = usernamTextField.getText();
        var password = passwordTextField.getText();
        var confirmPassword = confirmPasswordField.getText();
        var currentPassword = currentPasswordField.getText();

        var authenticator = new Authenticator();
        if(authenticator.authenticate(user.getUsername(), currentPassword) != Authenticator.StoreUserAuthResult.SUCCESS) {
            messageLabel.setText("La contraseña actual no es correcta");
            messageLabel.setVisible(true);
            return;
        }

        // Update user
        user.setName(name);
        user.setFirstSurname(firstSurname);
        user.setSecondSurname(secondSurname);
        user.setUsername(username);
        user.setPassword(password);

        try {
            var userRepository = new StoreUserRepository();

            if(!password.isEmpty()) {
                if(!password.equals(confirmPassword)) {
                    messageLabel.setText("Las contraseñas no coinciden");
                    messageLabel.setVisible(true);
                    return;
                }

                userRepository.updatePassword(user, password);
            }

            userRepository.update(user);

            messageLabel.setText("Usuario actualizado correctamente");
            messageLabel.setVisible(true);

            currentPasswordField.clear();
            passwordTextField.clear();
            confirmPasswordField.clear();
        }
        catch(Exception e) {
            messageLabel.setText("Error al actualizar el usuario");
            messageLabel.setVisible(true);
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        loadUserData();
    }
}
