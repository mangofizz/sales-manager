package com.salesmanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.App;
import com.salesmanager.Utils;
import com.salesmanager.data.Customer;
import com.salesmanager.data.CustomerRepository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class CustomersController implements Initializable {
    @FXML
    private JFXTextField customerSearchTextInput;
    @FXML
    private TableView<Customer> customersTable;
    @FXML
    private TableColumn<Customer, String> rfcTableColumn;
    @FXML
    private TableColumn<Customer, String> nameTableColumn;
    @FXML
    private TableColumn<Customer, String> emailTableColumn;
    @FXML
    private TableColumn<Customer, String> phoneTableColumn;

    private CustomerRepository customerRepository;
    private ObservableList<Customer> customers = FXCollections.observableArrayList();

    private void setTableColumns() {
        rfcTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("rfc"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("fullName"));
        emailTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("email"));
        phoneTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("phone"));
    }

    private void loadCustomersInformation() {
        var customersData = customerRepository.getAll();
        customers.clear();
        customers.addAll(customersData);
    }

    @FXML
    private void loadTableInformation() {
        if(customerSearchTextInput.getText().isEmpty()) {
            customersTable.setItems(customers);
        }
        else {
            ObservableList<Customer> customersData = FXCollections.observableArrayList();
            var search = customerSearchTextInput.getText().toLowerCase();
            for (var customer : customers) {
                if(customer.getRfc().toLowerCase().contains(search) || customer.getName().toLowerCase().contains(search)) {
                    customersData.add(customer);
                }
            }
            customersTable.setItems(customersData);
        }
    }

    private void openCustomerForm(CustomerFormController.FormMode mode, Customer customer) {
        try {
            var productFormLoader = App.getFXMLLoader("customer_form");
            Parent root = productFormLoader.load();
            var controller = (CustomerFormController)productFormLoader.getController();

            controller.configureForm(mode, customer);
                
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();

            loadCustomersInformation();
            loadTableInformation();
        }
        catch(IOException e) {
            System.out.println("No se pudo abrir el formulario del cliente");
            e.printStackTrace();
        }
    }

    @FXML
    private void addCustomerButton() {
        openCustomerForm(CustomerFormController.FormMode.CREATE, null);
    }

    @FXML
    private void modifyCustomerButton() {
        var customer = customersTable.getSelectionModel().getSelectedItem();
        if(customer != null) {
            openCustomerForm(CustomerFormController.FormMode.UPDATE, customer);
        }
        else {
            Utils.showAlert("Selecciona un cliente", "Selecciona un cliente de la tabla para modificarlo", AlertType.WARNING);
        }
    }

    @FXML
    private void deleteCustomerButton() {
        var customer = customersTable.getSelectionModel().getSelectedItem();
        if(customer != null) {
            var button = Utils.showAlert("Eliminar cliente", "¿Está seguro de eliminar el cliente seleccionado?", AlertType.CONFIRMATION);
            if(button.get() == ButtonType.OK) {
                customerRepository.delete(customer);
                loadCustomersInformation();
                loadTableInformation();
            }
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setTableColumns();
        loadCustomersInformation();
        loadTableInformation();
    }

    public CustomersController() {
        customerRepository = new CustomerRepository();
    }
}
