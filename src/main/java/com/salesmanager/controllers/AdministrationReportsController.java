package com.salesmanager.controllers;

import com.salesmanager.Utils;

import java.io.FileOutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import javax.swing.JFileChooser;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.salesmanager.data.ProductRepository;
import com.salesmanager.data.ProductSoldRepository;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert.AlertType;

public class AdministrationReportsController implements Initializable {

    @FXML
    private void generateSalesReportButton() {
       Document document = new Document();
       try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Guardar reporte de ventas");
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY | JFileChooser.SAVE_DIALOG);
            fileChooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
                @Override
                public boolean accept(java.io.File file) {
                    return file.getName().toLowerCase().endsWith(".pdf") || file.isDirectory();
                }

                @Override
                public String getDescription() {
                    return "PDF (*.pdf)";
                }
            });
            fileChooser.showSaveDialog(null);

            var filePath = fileChooser.getSelectedFile().getAbsolutePath();
            if(!filePath.endsWith(".pdf")) {
                filePath += ".pdf";
            }

            PdfWriter.getInstance(document, new FileOutputStream(filePath));
            
            document.setPageSize(PageSize.A4.rotate());
            document.setMargins(0, 0, 30, 40);
            
            document.open();

            Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
            Font subheaderFont = new Font(Font.FontFamily.HELVETICA, 11, Font.NORMAL);
            Font tableFont = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            
            Paragraph header = new Paragraph();
            header.add(new Phrase("Reporte de ventas", headerFont));
            var currentDate = LocalDate.now();
            var previousMonth = currentDate.minusMonths(1);
            var dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            var reportDate = String.format("\n(%s - %s)", previousMonth.format(dateFormat), currentDate.format(dateFormat));
            header.add(new Phrase(reportDate, subheaderFont));
            header.setAlignment(Element.ALIGN_CENTER);
            header.setSpacingAfter(35);
            document.add(header);

            PdfPTable table = new PdfPTable(14);

            PdfPCell cell;
            cell = new PdfPCell(new Phrase("Código", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nombre/Descripción del producto", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Cantidad", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(1);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Importe", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(1);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Cliente", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Fecha", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(2);
            table.addCell(cell);
            
            try {
                var repository = new ProductSoldRepository();
                var sales = repository.getAll();

                
                for (var sale : sales) {
                    if(sale.getSale().getDate().isBefore(previousMonth)) {
                        continue;
                    }

                    cell = new PdfPCell(new Phrase(sale.getProduct().getCode(), tableFont));
                    cell.setColspan(2);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(sale.getProduct().getName(), tableFont));
                    cell.setColspan(4);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.format("%d", sale.getAmount()), tableFont));
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.format("%.2f", sale.getTotal()), tableFont));
                    cell.setColspan(1);
                    table.addCell(cell);

                    var customer = sale.getSale().getCustomer();
                    var customerName = customer != null ? customer.getFullName() : "N/R";
                    cell = new PdfPCell(new Phrase(customerName, tableFont));
                    cell.setColspan(4);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(sale.getSale().getDate().toString(), tableFont));
                    cell.setColspan(2);
                    table.addCell(cell);
                }

                document.add(table);
            } 
            catch (DocumentException e) {
                e.printStackTrace();
            }
            document.close();

            Utils.showAlert("Reporte generado", "El reporte se ha generado correctamente..", AlertType.INFORMATION);
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void generateProductsReport() {
        Document document = new Document();
       try {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setDialogTitle("Guardar reporte de productos a surtir");
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY | JFileChooser.SAVE_DIALOG);
            fileChooser.setFileFilter(new javax.swing.filechooser.FileFilter() {
                @Override
                public boolean accept(java.io.File file) {
                    return file.getName().toLowerCase().endsWith(".pdf") || file.isDirectory();
                }

                @Override
                public String getDescription() {
                    return "PDF (*.pdf)";
                }
            });
            fileChooser.showSaveDialog(null);

            var filePath = fileChooser.getSelectedFile().getAbsolutePath();
            if(!filePath.endsWith(".pdf")) {
                filePath += ".pdf";
            }

            PdfWriter.getInstance(document, new FileOutputStream(filePath));
            
            document.setPageSize(PageSize.A4.rotate());
            document.setMargins(0, 0, 30, 40);
            
            document.open();

            Font headerFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
            Font subheaderFont = new Font(Font.FontFamily.HELVETICA, 11, Font.NORMAL);
            Font tableFont = new Font(Font.FontFamily.HELVETICA, 9, Font.BOLD);
            
            Paragraph header = new Paragraph();
            header.add(new Phrase("Reporte de productos a surtir", headerFont));
            var currentDate = LocalDate.now();
            var dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            var reportDate = String.format("\n(%s)", currentDate.format(dateFormat));
            header.add(new Phrase(reportDate, subheaderFont));
            header.setAlignment(Element.ALIGN_CENTER);
            header.setSpacingAfter(35);
            document.add(header);

            PdfPTable table = new PdfPTable(8);

            PdfPCell cell;
            cell = new PdfPCell(new Phrase("Código", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Nombre/Descripción del producto", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(4);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Cantidad", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(1);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Costo", tableFont));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(1);
            table.addCell(cell);
            
            try {
                var repository = new ProductRepository();
                var products = repository.getAll();

                
                for (var product : products) {
                    if(product.getStock() > 2) {
                        continue;
                    }

                    cell = new PdfPCell(new Phrase(product.getCode(), tableFont));
                    cell.setColspan(2);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(product.getName(), tableFont));
                    cell.setColspan(4);
                    table.addCell(cell);

                    var amount = product.getStock() == 0 ? "Agotado" : String.format("%d", product.getStock());
                    cell = new PdfPCell(new Phrase(amount, tableFont));
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.format("%.2f", product.getCost()), tableFont));
                    cell.setColspan(1);
                    table.addCell(cell);
                }

                document.add(table);
            } 
            catch (DocumentException e) {
                e.printStackTrace();
            }
            document.close();

            Utils.showAlert("Reporte generado", "El reporte se ha generado correctamente..", AlertType.INFORMATION);
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        
    }
}
