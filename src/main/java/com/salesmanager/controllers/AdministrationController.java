package com.salesmanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.salesmanager.App;
import com.salesmanager.data.StoreUser;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

public class AdministrationController implements Initializable {

    private enum AdministrationView {
        ACCOUNT, REPORTS, USERS
    }

    private AdministrationView currentView;

    @FXML
    private AnchorPane buttonsPane;
    @FXML
    private AnchorPane contentPane;
    @FXML
    private JFXButton accountButton;
    @FXML
    private JFXButton reportsButton;
    @FXML
    private JFXButton usersButton;

    @FXML
    private void accountsButtonAction() {
        if(currentView != AdministrationView.ACCOUNT) {
            currentView = AdministrationView.ACCOUNT;
            
            try {
                var content = App.loadFXML("admin_user_edit");
                var childern = contentPane.getChildren();
                childern.clear();
                childern.add(content);
            } 
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void reportsButtonAction() {
        if(currentView != AdministrationView.REPORTS) {
            currentView = AdministrationView.REPORTS;
            
            try {
                var content = App.loadFXML("admin_reports");
                var childern = contentPane.getChildren();
                childern.clear();
                childern.add(content);
            } 
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void usersButtonAction() {
        if(currentView != AdministrationView.USERS) {
            currentView = AdministrationView.USERS;
            
            try {
                var content = App.loadFXML("admin_users");
                var childern = contentPane.getChildren();
                childern.clear();
                childern.add(content);
            } 
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        if(DashboardController.userSession.getUser().getType() != StoreUser.UserType.ADMINISTRATOR) {
            buttonsPane.getChildren().remove(reportsButton);
            buttonsPane.getChildren().remove(usersButton);
        }

        accountsButtonAction();
    }

}
