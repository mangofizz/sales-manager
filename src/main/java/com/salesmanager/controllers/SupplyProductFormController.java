package com.salesmanager.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.data.Product;
import com.salesmanager.data.ProductRepository;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class SupplyProductFormController implements Initializable {

    @FXML
    private JFXTextField stockTextField;
    @FXML
    private Label stockInputErrorLabel;
    
    private Product product;

    private void hideErrorLabels() {
        stockInputErrorLabel.setVisible(false);
    }

    public void configureForm(Product product) {
        this.product = product;
    }

    @FXML
    private void closeForm() {
        Stage stage = (Stage)stockTextField.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void acceptButtonClick() {
        hideErrorLabels();

        if(stockTextField.getText().isEmpty()) {
            stockInputErrorLabel.setText("Requerido*");
            stockInputErrorLabel.setVisible(true);
            return;
        }

        try {
            Integer.parseInt(stockTextField.getText());
        }
        catch(NumberFormatException e) {
            stockInputErrorLabel.setText("Valor inválido");
            stockInputErrorLabel.setVisible(true);
            return;
        }

        ProductRepository productRepository = new ProductRepository();

        int amount = Integer.parseInt(stockTextField.getText());
        product.setStock(product.getStock() + amount);
        
        try {
            productRepository.update(product);
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        closeForm();
    }

    @FXML
    private void cancelButtonClick() {
        closeForm();
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // TODO Auto-generated method stub

    }
}
