package com.salesmanager.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.data.Customer;
import com.salesmanager.data.CustomerRepository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class CustomerSearchController implements Initializable {

    @FXML
    private JFXTextField customerSearchTextInput;
    @FXML
    private TableView<Customer> customersTable;
    @FXML
    private TableColumn<Customer, String> rfcTableColumn;
    @FXML
    private TableColumn<Customer, String> nameTableColumn;

    ICustomerCallback callbackChannel;

    private CustomerRepository customerRepository;
    private ObservableList<Customer> customers = FXCollections.observableArrayList();

    private void setTableColumns() {
        rfcTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("rfc"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<Customer, String>("fullName"));
    }

    private void loadCustomersInformation() {
        var customersData = customerRepository.getAll();
        customers.clear();
        customers.addAll(customersData);
    }

    @FXML
    private void loadTableInformation() {
        if(customerSearchTextInput.getText().isEmpty()) {
            customersTable.setItems(customers);
        }
        else {
            ObservableList<Customer> customersData = FXCollections.observableArrayList();
            var search = customerSearchTextInput.getText().toLowerCase();
            for (var customer : customers) {
                if(customer.getRfc().toLowerCase().contains(search) || customer.getName().toLowerCase().contains(search)) {
                    customersData.add(customer);
                }
            }
            customersTable.setItems(customersData);
        }
    }

    @FXML
    private void closeForm() {
        Stage stage = (Stage)customerSearchTextInput.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onAcceptButtonClick() {
        var product = customersTable.getSelectionModel().getSelectedItem();
        if(product != null) {
            callbackChannel.onCustomerSelected(product);
            closeForm();
        }
    }

    @FXML
    private void onCancelButtonClick() {
        closeForm();
    }
    
    public void configure(ICustomerCallback callback) {
        callbackChannel = callback;
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setTableColumns();
        loadCustomersInformation();
        loadTableInformation();
    }

    public CustomerSearchController() {
        customerRepository = new CustomerRepository();
    }

}
