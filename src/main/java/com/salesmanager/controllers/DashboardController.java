package com.salesmanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.salesmanager.App;
import com.salesmanager.IUserSession;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

public class DashboardController implements Initializable {
    
    public static IUserSession userSession;

    @FXML
    private Label userMessageLabel;
    @FXML
    private Pane contentPane;

    private void setUserMessage() {
        var user = userSession.getUser();
        var message = "Hola, " + user.getName() + "!";
        userMessageLabel.setText(message);
    }

    public static void setUserSession(IUserSession userSession) {
        DashboardController.userSession = userSession;
    }

    @FXML void inventoryButtonAction() {
        try {
            var content = App.loadFXML("inventory");
            var childern = contentPane.getChildren();
            childern.clear();
            childern.add(content);
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML void salesButtonAction() {
        try {
            var content = App.loadFXML("sale");
            var childern = contentPane.getChildren();
            childern.clear();
            childern.add(content);
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML void clientsButtonAction() {
        try {
            var content = App.loadFXML("clients");
            var childern = contentPane.getChildren();
            childern.clear();
            childern.add(content);
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML void adminButtonAction() {
        try {
            var content = App.loadFXML("admin_panel");
            var childern = contentPane.getChildren();
            childern.clear();
            childern.add(content);
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void logoutButtonClick() {
        userSession.destroy();
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setUserMessage();

        try {
            var content = App.loadFXML("inventory");
            contentPane.getChildren().add(content);
        } 
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
