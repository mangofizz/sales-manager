package com.salesmanager.controllers;

import com.salesmanager.data.Customer;

public interface ICustomerCallback {
    void onCustomerSelected(Customer client);
}
