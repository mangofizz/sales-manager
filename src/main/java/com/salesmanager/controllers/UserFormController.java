package com.salesmanager.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.salesmanager.Utils;
import com.salesmanager.data.StoreUser;
import com.salesmanager.data.StoreUserRepository;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class UserFormController implements Initializable {
    @FXML
    private JFXTextField nameTextField;
    @FXML
    private Label nameInputErrorLabel;
    @FXML
    private JFXTextField firstSurnameTextField;
    @FXML
    private Label firstSurnameInputErrorLabel;
    @FXML
    private JFXTextField secondSurnameTextField;
    @FXML
    private Label secondSurnameInputErrorLabel;
    @FXML
    private JFXTextField usernameTextField;
    @FXML
    private Label usernameInputErrorLabel;
    @FXML
    private JFXPasswordField passwordTextField;
    @FXML
    private Label passwordInputErrorLabel;
    @FXML
    private JFXPasswordField confirmPasswordTextField;
    @FXML
    private Label confirmPasswordInputErrorLabel;
    @FXML
    private JFXComboBox<String> userTypeComboBox;

    public enum FormMode {
        CREATE,
        UPDATE
    }

    private StoreUser user;
    private FormMode formMode;

    private void hideErrorLabels() {
        nameInputErrorLabel.setVisible(false);
        firstSurnameInputErrorLabel.setVisible(false);
        secondSurnameInputErrorLabel.setVisible(false);
        usernameInputErrorLabel.setVisible(false);
        passwordInputErrorLabel.setVisible(false);
        confirmPasswordInputErrorLabel.setVisible(false);
    }

    public void configureForm(FormMode mode, StoreUser user) {
        this.formMode = mode;
        this.user = user;

        if(mode == FormMode.UPDATE) {
            nameTextField.setText(user.getName());
            firstSurnameTextField.setText(user.getFirstSurname());
            secondSurnameTextField.setText(user.getSecondSurname());
            usernameTextField.setText(user.getUsername());
            if(user.getType() == StoreUser.UserType.ADMINISTRATOR) {
                userTypeComboBox.getSelectionModel().select(0);
            }
            else {
                userTypeComboBox.getSelectionModel().select(1);
            }
        }
    }

    @FXML
    private void closeForm() {
        Stage stage = (Stage) nameTextField.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void acceptButtonClick() {
        hideErrorLabels();

        if(formMode == FormMode.CREATE) {
            user = new StoreUser();
        }

        boolean isValid = true;

        if(nameTextField.getText().isEmpty()) {
            nameInputErrorLabel.setText("Requerido*");
            nameInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(firstSurnameTextField.getText().isEmpty()) {
            firstSurnameInputErrorLabel.setText("Requerido*");
            firstSurnameInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(secondSurnameTextField.getText().isEmpty()) {
            secondSurnameInputErrorLabel.setText("Requerido*");
            secondSurnameInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(usernameTextField.getText().isEmpty()) {
            usernameInputErrorLabel.setText("Requerido*");
            usernameInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(formMode == FormMode.CREATE || !passwordTextField.getText().isEmpty()) {
            if(passwordTextField.getText().isEmpty()) {
                passwordInputErrorLabel.setText("Requerido*");
                passwordInputErrorLabel.setVisible(true);
                isValid = false;
            }
    
            if(confirmPasswordTextField.getText().isEmpty()) {
                confirmPasswordInputErrorLabel.setText("Requerido*");
                confirmPasswordInputErrorLabel.setVisible(true);
                isValid = false;
            }
            else {
                if(!passwordTextField.getText().equals(confirmPasswordTextField.getText())) {
                    confirmPasswordInputErrorLabel.setText("Las contraseñas no coinciden");
                    confirmPasswordInputErrorLabel.setVisible(true);
                    isValid = false;
                }
            }
        }

        if(!isValid) {
            return;
        }

        StoreUserRepository repository = new StoreUserRepository();

        user.setName(Utils.toFirstWordLetterUppercaseString(nameTextField.getText()));
        user.setFirstSurname(Utils.toFirstWordLetterUppercaseString(firstSurnameTextField.getText()));
        user.setSecondSurname(Utils.toFirstWordLetterUppercaseString(secondSurnameTextField.getText()));
        user.setUsername(usernameTextField.getText());
        user.setType(userTypeComboBox.getSelectionModel().getSelectedIndex() == 0 ? StoreUser.UserType.ADMINISTRATOR : StoreUser.UserType.EMPLOYEE);

        if(formMode == FormMode.CREATE) {
            user.setPassword(passwordTextField.getText());
            repository.create(user);
        }
        else {
            repository.update(user);

            if(passwordTextField.getText().length() > 0) {
                repository.updatePassword(user, passwordTextField.getText());
            }
        }

        closeForm();
    }

    @FXML
    private void cancelButtonClick() {
        closeForm();
    }
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        userTypeComboBox.getItems().add("Administrador");
        userTypeComboBox.getItems().add("Empleado");
        userTypeComboBox.getSelectionModel().select(0);
    }
}
