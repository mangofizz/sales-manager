package com.salesmanager.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.data.Product;
import com.salesmanager.data.ProductRepository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

public class ProductSearchController implements Initializable {

    @FXML
    private JFXTextField productSearchTextInput;
    @FXML
    private TableView<Product> productsTable;
    @FXML
    private TableColumn<Product, String> codeTableColumn;
    @FXML
    private TableColumn<Product, String> nameTableColumn;
    @FXML
    private TableColumn<Product, String> priceTableColumn;
    
    IProductCallback callbackChannel;

    private ProductRepository productRepository;
    private ObservableList<Product> products = FXCollections.observableArrayList();

    private void setTableColumns() {
        codeTableColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("code"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        priceTableColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("price"));
    }

    private void loadProductsInformation() {
        var productsData = productRepository.getAll();
        products.clear();
        products.addAll(productsData);
    }

    @FXML
    private void loadTableInformation() {
        if(productSearchTextInput.getText().isEmpty()) {
            productsTable.setItems(products);
        }
        else {
            ObservableList<Product> productsData = FXCollections.observableArrayList();
            var search = productSearchTextInput.getText().toLowerCase();
            for (var product : products) {
                if(product.getCode().toLowerCase().contains(search) || product.getName().toLowerCase().contains(search)) {
                    productsData.add(product);
                }
            }
            productsTable.setItems(productsData);
        }
    }

    @FXML
    private void closeForm() {
        Stage stage = (Stage)productSearchTextInput.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void onAcceptButtonClick() {
        var product = productsTable.getSelectionModel().getSelectedItem();
        if(product != null) {
            callbackChannel.onProductSelected(product);
            closeForm();
        }
    }

    @FXML
    private void onCancelButtonClick() {
        closeForm();
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setTableColumns();
        loadProductsInformation();
        loadTableInformation();
    }
    
    public void configure(IProductCallback callback) {
        callbackChannel = callback;
    }

    public ProductSearchController() {
        productRepository = new ProductRepository();
    }

}
