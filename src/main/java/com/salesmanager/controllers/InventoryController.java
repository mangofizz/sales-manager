package com.salesmanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.App;
import com.salesmanager.Utils;
import com.salesmanager.data.Product;
import com.salesmanager.data.ProductRepository;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class InventoryController implements Initializable {

    @FXML
    private JFXTextField productSearchTextInput;
    @FXML
    private TableView<Product> inventoryTable;
    @FXML
    private TableColumn<Product, String> codeTableColumn;
    @FXML
    private TableColumn<Product, String> nameTableColumn;
    @FXML
    private TableColumn<Product, String> costTableColumn;
    @FXML
    private TableColumn<Product, String> priceTableColumn;
    @FXML
    private TableColumn<Product, String> stockTableColumn;

    private ProductRepository productRepository;
    private ObservableList<Product> products = FXCollections.observableArrayList();

    private void setTableColumns() {
        codeTableColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("code"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
        costTableColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("cost"));
        priceTableColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("price"));
        stockTableColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("stock"));
    }

    private void loadProductsInformation() {
        var productsData = productRepository.getAll();
        products.clear();
        products.addAll(productsData);
    }

    @FXML
    private void loadTableInformation() {
        if(productSearchTextInput.getText().isEmpty()) {
            inventoryTable.setItems(products);
        }
        else {
            ObservableList<Product> productsData = FXCollections.observableArrayList();
            var search = productSearchTextInput.getText().toLowerCase();
            for (var product : products) {
                if(product.getCode().toLowerCase().contains(search) || product.getName().toLowerCase().contains(search)) {
                    productsData.add(product);
                }
            }
            inventoryTable.setItems(productsData);
        }
    }

    private void openProductForm(ProductFormController.FormMode mode, Product product) {
        try {
            var productFormLoader = App.getFXMLLoader("product_form");
            Parent root = productFormLoader.load();
            var controller = (ProductFormController)productFormLoader.getController();

            controller.configureForm(mode, product);
                
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();

            loadProductsInformation();
            loadTableInformation();
        }
        catch(IOException e) {
            System.out.println("No se pudo abrir el formulario de productos");
            e.printStackTrace();
        }
    }

    @FXML
    private void addProductButton() {
        openProductForm(ProductFormController.FormMode.CREATE, null);
    }

    @FXML
    private void modifyProductButton() {
        var product = inventoryTable.getSelectionModel().getSelectedItem();
        if(product != null) {
            openProductForm(ProductFormController.FormMode.UPDATE, product);
        }
        else {
            Utils.showAlert("Advertencia", "No se ha seleccionado un producto", AlertType.WARNING);
        }
    }

    @FXML
    private void deleteProductButton() {
        var product = inventoryTable.getSelectionModel().getSelectedItem();
        if(product != null) {
            var button = Utils.showAlert("Eliminar producto", "¿Está seguro de eliminar el producto seleccionado?", AlertType.CONFIRMATION);
            if(button.get() == javafx.scene.control.ButtonType.OK) {
                productRepository.delete(product);
                loadProductsInformation();
            }
        }
    }

    @FXML
    private void supplyProduct() {
        var product = inventoryTable.getSelectionModel().getSelectedItem();
        if(product != null) {
            try {
                var supplyFormLoader = App.getFXMLLoader("supply_product_form");
                Parent root = supplyFormLoader.load();
                var controller = (SupplyProductFormController)supplyFormLoader.getController();

                controller.configureForm(product);
                    
                Scene scene = new Scene(root);
                Stage stage = new Stage();
                stage.setScene(scene);
                stage.initModality(Modality.APPLICATION_MODAL);
                stage.showAndWait();

                loadProductsInformation();
                loadTableInformation();
            }
            catch(IOException e) {
                System.out.println("No se pudo abrir el formulario de productos");
                e.printStackTrace();
            }
        }
        else {
            Utils.showAlert("Advertencia", "No se ha seleccionado un producto", AlertType.WARNING);
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setTableColumns();
        loadProductsInformation();
        loadTableInformation();
    }

    public InventoryController() {
        this.productRepository = new ProductRepository();
    }
    
}
