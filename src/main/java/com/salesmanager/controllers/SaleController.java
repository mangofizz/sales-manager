package com.salesmanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.Random;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.App;
import com.salesmanager.Utils;
import com.salesmanager.data.Customer;
import com.salesmanager.data.Product;
import com.salesmanager.data.ProductSold;
import com.salesmanager.data.Sale;
import com.salesmanager.data.SaleRepository;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SaleController implements Initializable, IProductCallback, ICustomerCallback {

    @FXML
    private JFXTextField productSearchTextInput;
    @FXML
    private TableView<ProductSold> productsTable;
    @FXML
    private TableColumn<ProductSold, String> codeTableColumn;
    @FXML
    private TableColumn<ProductSold, String> nameTableColumn;
    @FXML
    private TableColumn<ProductSold, String> priceTableColumn;
    @FXML
    private TableColumn<ProductSold, String> amountTableColumn;
    @FXML
    private TableColumn<ProductSold, String> totalTableColumn;

    @FXML
    private TextField customerFullNameField;
    @FXML
    private TextField customerEmailField;
    @FXML
    private TextField customerPhoneField;
    @FXML
    private TextField customerRfcField;

    @FXML
    private TextField productName;
    @FXML
    private TextField productCode;
    @FXML
    private TextField productPrice;
    @FXML
    private JFXTextField productAmount;

    @FXML
    private Text totalText;

    private ObservableList<ProductSold> products = FXCollections.observableArrayList();
    private Product selectedProduct;
    private Customer selectedCustomer;

    private void setTableColumns() {
        codeTableColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProduct().getCode()));
        nameTableColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProduct().getName()));
        priceTableColumn.setCellValueFactory(cellData -> new SimpleStringProperty(String.format("%.2f", cellData.getValue().getProduct().getPrice())));
        amountTableColumn.setCellValueFactory(new PropertyValueFactory<ProductSold, String>("amount"));
        totalTableColumn.setCellValueFactory(new PropertyValueFactory<ProductSold, String>("total"));
    }

    @FXML
    private void loadTableInformation() {
        if(productSearchTextInput.getText().isEmpty()) {
            productsTable.setItems(products);
        }
        else {
            ObservableList<ProductSold> productsData = FXCollections.observableArrayList();
            var search = productSearchTextInput.getText().toLowerCase();
            for (var soldProduct : products) {
                var product = soldProduct.getProduct();
                if(product.getCode().toLowerCase().contains(search) || product.getName().toLowerCase().contains(search)) {
                    productsData.add(soldProduct);
                }
            }
            productsTable.setItems(productsData);
        }
    }

    private void calculateTotal() {
        var total = 0.0;
        for (var product : products) {
            total += product.getTotal();
        }
        totalText.setText(String.format("Total: $%.2f", total));
    }
    
    private void cleanSelectedProduct() {
        productName.setText("");
        productCode.setText("");
        productPrice.setText("");
        productAmount.setText("");
        selectedProduct = null;
    }

    @FXML
    private void addProductToSale() {
        if (selectedProduct != null) {
            if(productAmount.getText().isEmpty()) {
                Utils.showAlert("Aviso", "Por favor introduzca una cantidad válida.", AlertType.ERROR);
                return;
            }
            else {
                try {
                    Integer.parseInt(productAmount.getText());
                }
                catch(NumberFormatException e) {
                    Utils.showAlert("Aviso", "Por favor introduzca una cantidad válida.", AlertType.ERROR);
                    return;
                }
            }

            // Check for stock
            if(selectedProduct.getStock() < Integer.parseInt(productAmount.getText())) {
                var message = String.format("No hay suficientes existencias para el producto seleccionado. \nCantidad disponible: %d", selectedProduct.getStock());
                Utils.showAlert("Aviso", message, AlertType.WARNING);
                return;
            }

            for (var soldProduct : products) {
                if(soldProduct.getProduct().getId() == selectedProduct.getId()) {
                    soldProduct.setAmount(soldProduct.getAmount() + Integer.parseInt(productAmount.getText()));
                    productsTable.refresh();
                    cleanSelectedProduct();
                    calculateTotal();
                    return;
                }
            }

            var productSold = new ProductSold();
            productSold.setProduct(selectedProduct);
            productSold.setAmount(Integer.parseInt(productAmount.getText()));
            products.add(productSold);
            loadTableInformation();
            cleanSelectedProduct();
            calculateTotal();
        }
        else {
            Utils.showAlert("Aviso", "Por favor seleccione un producto.", AlertType.ERROR);
        }
    }

    @FXML
    private void selectClientButtonAction() {
        try {
            var productFormLoader = App.getFXMLLoader("clients_search");
            Parent root = productFormLoader.load();
            var controller = (CustomerSearchController)productFormLoader.getController();
            controller.configure(this);

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Seleccionar client");
            stage.setScene(scene);
            stage.showAndWait();

            loadTableInformation();
        }
        catch(IOException e) {
            System.out.println("No se pudo abrir el formulario de productos");
            e.printStackTrace();
        }
    }

    @FXML
    private void searchProductButtonAction() {
        try {
            var productFormLoader = App.getFXMLLoader("product_search");
            Parent root = productFormLoader.load();
            var controller = (ProductSearchController)productFormLoader.getController();
            controller.configure(this);

            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setTitle("Buscar producto");
            stage.setScene(scene);
            stage.showAndWait();

            loadTableInformation();
        }
        catch(IOException e) {
            System.out.println("No se pudo abrir el formulario de productos");
            e.printStackTrace();
        }
    }

    @FXML
    private void deleteProductButton() {
        var product = productsTable.getSelectionModel().getSelectedItem();
        if(product != null) {
            var button = Utils.showAlert("Confirmación", "¿Está seguro que desea eliminar el producto seleccionado?", AlertType.CONFIRMATION);
            if(button.get() == ButtonType.OK) {
                products.remove(product);
                loadTableInformation();
                calculateTotal();
            }
        }
        else {
            Utils.showAlert("Aviso", "Por favor seleccione un producto.", AlertType.ERROR);
        }
    }

    @FXML
    private void cancelSaleButton() {
        if(!products.isEmpty()) {
            var button = Utils.showAlert("Cancelar venta", "¿Está seguro que desea cancelar la venta?", AlertType.CONFIRMATION);
            if(button.get() == ButtonType.OK) {
                products.clear();
                loadTableInformation();
                cleanSelectedProduct();
                totalText.setText("Total: $--.--");
            }
        }
        else {
            Utils.showAlert("Cancelar venta", "No se han agregado productos.", AlertType.ERROR);
        }
    }

    @FXML
    private void generateSaleButton() {
        if(!products.isEmpty()) {
            var button = Utils.showAlert("Confirmación", "¿Está seguro que desea finalizar la venta?", AlertType.CONFIRMATION);
            if(button.get() == ButtonType.OK) {
                var sale = new Sale();
                sale.setCustomer(selectedCustomer);
                sale.setDate(LocalDate.now());
                sale.setStoreUser(DashboardController.userSession.getUser());
                
                // set a random 6 digit number
                var random = new Random();
                var number = random.nextInt(999999);
                sale.setNumber(number);
                
                ProductSold saleProducts[] = new ProductSold[products.size()];
                products.toArray(saleProducts);
                sale.setProductsSold(saleProducts);

                SaleRepository saleRepository = new SaleRepository();
                saleRepository.create(sale);

                products.clear();
                loadTableInformation();
                cleanSelectedProduct();
                totalText.setText("Total: $--.--");
            }
        }
        else {
            Utils.showAlert("Aviso", "No se han agregado productos.", AlertType.INFORMATION);
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setTableColumns();
        loadTableInformation();
    }

    public SaleController() {

    }

    @Override
    public void onCustomerSelected(Customer customer) {
        selectedCustomer = customer;
        customerFullNameField.setText(customer.getFullName());
        customerEmailField.setText(customer.getEmail());
        customerPhoneField.setText(customer.getPhone());
        customerRfcField.setText(customer.getRfc());
    }

    @Override
    public void onProductSelected(Product product) {
        selectedProduct = product;
        productName.setText(product.getName());
        productCode.setText(product.getCode());
        productPrice.setText(String.format("%.2f", product.getPrice()));
        productAmount.setText("1");
    }
    
}
