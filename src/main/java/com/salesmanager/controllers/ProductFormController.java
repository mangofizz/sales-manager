package com.salesmanager.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.data.Product;
import com.salesmanager.data.ProductRepository;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ProductFormController implements Initializable {

    @FXML
    private JFXTextField nameTextField;
    @FXML
    private Label nameInputErrorLabel;
    @FXML
    private JFXTextField codeTextField;
    @FXML
    private Label codeInputErrorLabel;
    @FXML
    private JFXTextField costTextField;
    @FXML
    private Label costInputErrorLabel;
    @FXML
    private JFXTextField priceTextField;
    @FXML
    private Label priceInputErrorLabel;
    @FXML
    private JFXTextField stockTextField;
    @FXML
    private Label stockInputErrorLabel;

    public enum FormMode {
        CREATE,
        UPDATE
    }

    private Product product;
    private FormMode formMode;

    private void hideErrorLabels() {
        nameInputErrorLabel.setVisible(false);
        codeInputErrorLabel.setVisible(false);
        costInputErrorLabel.setVisible(false);
        priceInputErrorLabel.setVisible(false);
        stockInputErrorLabel.setVisible(false);
    }

    public void configureForm(FormMode mode, Product product) {
        this.formMode = mode;
        this.product = product;

        if(mode == FormMode.UPDATE) {
            nameTextField.setText(product.getName());
            codeTextField.setText(product.getCode());
            costTextField.setText(String.valueOf(product.getCost()));
            priceTextField.setText(String.valueOf(product.getPrice()));
            stockTextField.setText(String.valueOf(product.getStock()));
        }
    }

    @FXML
    private void closeForm() {
        Stage stage = (Stage)nameTextField.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void acceptButtonClick() {
        hideErrorLabels();

        if(formMode == FormMode.CREATE) {
            product = new Product();
            codeTextField.setDisable(false);
        }
        else {
            codeTextField.setDisable(true);
        }

        boolean inputIsValid = true;

        if(nameTextField.getText().isEmpty()) {
            nameInputErrorLabel.setText("Requerido*");
            nameInputErrorLabel.setVisible(true);
            inputIsValid = false;
        }

        if(codeTextField.getText().isEmpty()) {
            codeInputErrorLabel.setText("Requerido*");
            codeInputErrorLabel.setVisible(true);
            inputIsValid = false;
        }

        if(costTextField.getText().isEmpty()) {
            costInputErrorLabel.setText("Requerido*");
            costInputErrorLabel.setVisible(true);
            inputIsValid = false;
        }

        if(priceTextField.getText().isEmpty()) {
            priceInputErrorLabel.setText("Requerido*");
            priceInputErrorLabel.setVisible(true);
            inputIsValid = false;
        }

        if(stockTextField.getText().isEmpty()) {
            stockInputErrorLabel.setText("Requerido*");
            stockInputErrorLabel.setVisible(true);
            inputIsValid = false;
        }

        try {
            Float.parseFloat(costTextField.getText());
        }
        catch(NumberFormatException e) {
            costInputErrorLabel.setText("Valor inválido");
            costInputErrorLabel.setVisible(true);
            inputIsValid = false;
        }

        try {
            Float.parseFloat(priceTextField.getText());
        }
        catch(NumberFormatException e) {
            priceInputErrorLabel.setText("Valor inválido");
            priceInputErrorLabel.setVisible(true);
            inputIsValid = false;
        }

        try {
            Integer.parseInt(stockTextField.getText());
        }
        catch(NumberFormatException e) {
            stockInputErrorLabel.setText("Valor inválido");
            stockInputErrorLabel.setVisible(true);
            inputIsValid = false;
        }

        if(!inputIsValid) {
            return;
        }

        ProductRepository productRepository = new ProductRepository();

        if(formMode == FormMode.CREATE) {
            if(productRepository.exists(codeTextField.getText())) {
                codeInputErrorLabel.setText("Código duplicado");
                codeInputErrorLabel.setVisible(true);
                return;
            }
        }

        product.setName(nameTextField.getText());
        product.setCode(codeTextField.getText());
        product.setCost(Float.parseFloat(costTextField.getText()));
        product.setPrice(Float.parseFloat(priceTextField.getText()));
        product.setStock(Integer.parseInt(stockTextField.getText()));
        
        if(formMode == FormMode.CREATE) {
            productRepository.create(product);
        } 
        else {
            productRepository.update(product);
        }

        closeForm();
    }

    @FXML
    private void cancelButtonClick() {
        closeForm();
    }
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub
        
    }
}
