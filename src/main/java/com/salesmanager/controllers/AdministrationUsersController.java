package com.salesmanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.App;
import com.salesmanager.Utils;
import com.salesmanager.data.StoreUser;
import com.salesmanager.data.StoreUserRepository;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AdministrationUsersController implements Initializable {
    
    @FXML
    private JFXTextField usersSearchTextInput;
    @FXML
    private TableView<StoreUser> usersTable;
    @FXML
    private TableColumn<StoreUser, String> usernameTableColumn;
    @FXML
    private TableColumn<StoreUser, String> fullNameTableColumn;
    @FXML
    private TableColumn<StoreUser, String> typeTableColumn;

    private StoreUserRepository storeUserRepository;
    private ObservableList<StoreUser> users = FXCollections.observableArrayList();

    private void setTableColumns() {
        usernameTableColumn.setCellValueFactory(new PropertyValueFactory<StoreUser, String>("username"));
        fullNameTableColumn.setCellValueFactory(new PropertyValueFactory<StoreUser, String>("fullName"));
        typeTableColumn.setCellValueFactory(cellData -> {
            var type = cellData.getValue().getType();
            var typeString = type.getValue() == 0 ? "Administrador" : "Vendedor";
            return new SimpleStringProperty(typeString);
        });
    }

    private void loadUsersInformation() {
        var usersData = storeUserRepository.getAll();
        users.clear();
        users.addAll(usersData);
    }

    @FXML
    private void loadTableInformation() {
        if(usersSearchTextInput.getText().isEmpty()) {
            usersTable.setItems(users);
        }
        else {
            ObservableList<StoreUser> usersData = FXCollections.observableArrayList();
            var search = usersSearchTextInput.getText().toLowerCase();
            for (var user : users) {
                if(user.getUsername().toLowerCase().contains(search) || user.getFullName().toLowerCase().contains(search)) {
                    usersData.add(user);
                }
            }
            usersTable.setItems(usersData);
        }
    }

    @FXML
    private void openUserForm(UserFormController.FormMode mode, StoreUser user) {
        try {
            var productFormLoader = App.getFXMLLoader("user_form");
            Parent root = productFormLoader.load();
            var controller = (UserFormController)productFormLoader.getController();

            controller.configureForm(mode, user);
                
            Scene scene = new Scene(root);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();

            loadUsersInformation();
            loadTableInformation();
        }
        catch(IOException e) {
            System.out.println("No se pudo abrir el formulario del usuario");
            e.printStackTrace();
        }
    }

    @FXML
    private void openUserForm() {
        openUserForm(UserFormController.FormMode.CREATE, null);
    }

    @FXML
    private void openUserFormForEdit() {
        var user = usersTable.getSelectionModel().getSelectedItem();
        if(user != null) {
            openUserForm(UserFormController.FormMode.UPDATE, user);
        }
        else {
            Utils.showAlert("No se ha seleccionado un usuario", "Por favor seleccione un usuario de la tabla", AlertType.WARNING);
        }
    }

    @FXML
    private void deleteUser() {
        var user = usersTable.getSelectionModel().getSelectedItem();
        if(user != null) {
            var button = Utils.showAlert("Eliminar usuario", "¿Está seguro que desea eliminar el usuario?", AlertType.CONFIRMATION);
            if(button.get() == ButtonType.OK) {
                storeUserRepository.delete(user);
                loadUsersInformation();
                loadTableInformation();
            }
        }
        else {
            Utils.showAlert("No se ha seleccionado un usuario", "Por favor seleccione un usuario de la tabla", AlertType.WARNING);
        }
    }

    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        setTableColumns();
        loadUsersInformation();
        loadTableInformation();
    }

    public AdministrationUsersController() {
        storeUserRepository = new StoreUserRepository();
    }
    
}
