package com.salesmanager.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXTextField;
import com.salesmanager.Utils;
import com.salesmanager.data.Customer;
import com.salesmanager.data.CustomerRepository;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class CustomerFormController implements Initializable {
    @FXML
    private JFXTextField nameTextField;
    @FXML
    private Label nameInputErrorLabel;
    @FXML
    private JFXTextField firstSurnameTextField;
    @FXML
    private Label firstSurnameInputErrorLabel;
    @FXML
    private JFXTextField secondSurnameTextField;
    @FXML
    private Label secondSurnameInputErrorLabel;
    @FXML
    private JFXTextField rfcTextField;
    @FXML
    private Label rfcInputErrorLabel;
    @FXML
    private JFXTextField emailTextField;
    @FXML
    private Label emailInputErrorLabel;
    @FXML
    private JFXTextField phoneTextField;
    @FXML
    private Label phoneInputErrorLabel;

    public enum FormMode {
        CREATE,
        UPDATE
    }

    private Customer customer;
    private FormMode formMode;

    private void hideErrorLabels() {
        nameInputErrorLabel.setVisible(false);
        firstSurnameInputErrorLabel.setVisible(false);
        secondSurnameInputErrorLabel.setVisible(false);
        rfcInputErrorLabel.setVisible(false);
        emailInputErrorLabel.setVisible(false);
        phoneInputErrorLabel.setVisible(false);
    }

    public void configureForm(FormMode mode, Customer customer) {
        this.formMode = mode;
        this.customer = customer;

        if(mode == FormMode.UPDATE) {
            nameTextField.setText(customer.getName());
            firstSurnameTextField.setText(customer.getFirstSurname());
            secondSurnameTextField.setText(customer.getSecondSurname());
            rfcTextField.setText(customer.getRfc());
            emailTextField.setText(customer.getEmail());
            phoneTextField.setText(customer.getPhone());
        }
    }

    @FXML
    private void closeForm() {
        Stage stage = (Stage) nameTextField.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void acceptButtonClick() {
        hideErrorLabels();

        if(formMode == FormMode.CREATE) {
            customer = new Customer();
            rfcTextField.setDisable(false);
        }
        else {
            rfcTextField.setDisable(true);
        }

        boolean isValid = true;

        if(nameTextField.getText().isEmpty()) {
            nameInputErrorLabel.setText("Requerido*");
            nameInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(firstSurnameTextField.getText().isEmpty()) {
            firstSurnameInputErrorLabel.setText("Requerido*");
            firstSurnameInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(secondSurnameTextField.getText().isEmpty()) {
            secondSurnameInputErrorLabel.setText("Requerido*");
            secondSurnameInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(rfcTextField.getText().isEmpty()) {
            rfcInputErrorLabel.setText("Requerido*");
            rfcInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(emailTextField.getText().isEmpty()) {
            emailInputErrorLabel.setText("Requerido*");
            emailInputErrorLabel.setVisible(true);
            isValid = false;
        }
        else if(!emailTextField.getText().matches("^[A-Za-z0-9+_.-]+@(.+)$")) {
            emailInputErrorLabel.setText("Formato inválido");
            emailInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(phoneTextField.getText().isEmpty()) {
            phoneInputErrorLabel.setText("Requerido*");
            phoneInputErrorLabel.setVisible(true);
            isValid = false;
        }
        else if(!phoneTextField.getText().matches("[0-9]+")) {
            phoneInputErrorLabel.setText("Solo números");
            phoneInputErrorLabel.setVisible(true);
            isValid = false;
        }

        if(!isValid) {
            return;
        }

        CustomerRepository repository = new CustomerRepository();

        // Validate RFC
        if(!rfcTextField.getText().isEmpty()) {
            if(!rfcTextField.getText().matches("[A-Z]{4}[0-9]{6}[A-Z0-9]{3}")) {
                rfcInputErrorLabel.setText("Formato inválido");
                rfcInputErrorLabel.setVisible(true);
                return;
            }

            // Validate RFC matches with name and surnames
            String rfc = rfcTextField.getText().toUpperCase();
            String name = nameTextField.getText().toUpperCase();
            String firstSurname = firstSurnameTextField.getText().toUpperCase();
            String secondSurname = secondSurnameTextField.getText().toUpperCase();
            if(!rfc.substring(0, 4).equals(firstSurname.substring(0, 2) + secondSurname.substring(0, 1) + name.substring(0, 1))) {
                rfcInputErrorLabel.setText("No coincide con el nombre");
                rfcInputErrorLabel.setVisible(true);
                return;
            }

            if(formMode == FormMode.CREATE) {
                if(repository.exists(rfcTextField.getText())) {
                    rfcInputErrorLabel.setText("RFC ya registrado");
                    rfcInputErrorLabel.setVisible(true);
                    return;
                }
            }
        }

        customer.setName(Utils.toFirstWordLetterUppercaseString(nameTextField.getText()));
        customer.setFirstSurname(Utils.toFirstWordLetterUppercaseString(firstSurnameTextField.getText()));
        customer.setSecondSurname(Utils.toFirstWordLetterUppercaseString(secondSurnameTextField.getText()));
        customer.setRfc(rfcTextField.getText().toUpperCase());
        customer.setEmail(emailTextField.getText());
        customer.setPhone(phoneTextField.getText());

        if(formMode == FormMode.CREATE) {
            repository.create(customer);
        }
        else {
            repository.update(customer);
        }

        closeForm();
    }

    @FXML
    private void cancelButtonClick() {
        closeForm();
    }
    
    @Override
    public void initialize(URL arg0, ResourceBundle arg1) {
        // TODO Auto-generated method stub
        
    }
}
