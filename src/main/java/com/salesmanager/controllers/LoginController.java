package com.salesmanager.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.salesmanager.App;
import com.salesmanager.Authenticator;
import com.salesmanager.IUserSession;
import com.salesmanager.Utils;
import com.salesmanager.data.StoreUser;
import com.salesmanager.data.StoreUserRepository;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;

public class LoginController implements Initializable, IUserSession {

    private StoreUser loggedUser;

    @FXML
    private TextField tfUsername;
    @FXML
    private PasswordField pfPassword;
    @FXML
    private Label lbUserInputError;
    @FXML
    private Label lbPasswordInputError;

    @FXML
    private void loginButtonClick(ActionEvent event) {
        // Clear error labels
        lbUserInputError.setText(null);
        lbPasswordInputError.setText(null);
        
        var username = tfUsername.getText();
        var password = pfPassword.getText();
        var valid = true;
        
        if(username.isEmpty()) {
            lbUserInputError.setText("Campo obligatorio*");
            valid = false;
        }
        
        if(password.isEmpty()) {
            lbPasswordInputError.setText("Campo obligatorio*");
            valid = false;
        }
        
        if(valid) {
            var authenticator = new Authenticator();
            var result = authenticator.authenticate(username, password);

            switch(result) {
                case SUCCESS: {
                    var usersRepository = new StoreUserRepository();
                    this.loggedUser = usersRepository.getByUsername(username);
                    try {
                        DashboardController.setUserSession(this);
                        App.setScene("dashboard");
                    } catch (IOException e) {
                        System.out.println("Error al cargar la vista principal.");
                        e.printStackTrace();
                    }
                    break;
                }
                
                case INVALID_CREDENTIALS: {
                    lbUserInputError.setText("El usuario no existe.");
                    break;
                }
                
                case INCORRECT_PASSWORD: {
                    lbPasswordInputError.setText("La contraseña es incorrecta.");
                    break;
                }
                
                case DATABASE_ERROR: {
                    Utils.showAlert("Error de conexión", "Ocurrió un error al conectar con la base de datos.", AlertType.ERROR);
                    break;
                }
            }
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO Auto-generated method stub
    }

    @Override
    public StoreUser getUser() {
        return this.loggedUser;
    }

    @Override
    public void destroy() {
        this.loggedUser = null;
        try {
            App.setScene("login");
        }
        catch(IOException e) {
            System.out.println("Error al cargar la pantalla de inicio de sesión.");
        }
    }
    
}
