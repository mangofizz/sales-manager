package com.salesmanager.controllers;

import com.salesmanager.data.Product;

public interface IProductCallback {
    void onProductSelected(Product product);
}
