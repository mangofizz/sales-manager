package com.salesmanager;

import java.util.Properties;

public class Config {
    private static final String APPLICATION_CONFIGS_FILE_PATH = "com/salesmanager/application.properties";
    
    private Properties applicationConfigs;

    public Config() {
        try {
            var applicationConfigsFile = App.class.getClassLoader().getResourceAsStream(APPLICATION_CONFIGS_FILE_PATH);
            this.applicationConfigs = new Properties();
            applicationConfigs.load(applicationConfigsFile);
            applicationConfigsFile.close();
        }
        catch(Exception e) {
            System.err.println("Ocurrió un error al leer el archivo de configuraciones de la aplicación");
            System.err.println("Excepción: " + e.getMessage());
        }
    }
    
    public String getConfig(String configName) {
        return this.applicationConfigs.getProperty(configName);
    }
}
