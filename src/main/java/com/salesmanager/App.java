package com.salesmanager;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Stage stage;
    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        App.stage = stage;
        stage.setTitle("Gestor de ventas");
        scene = new Scene(loadFXML("login"));
        stage.setScene(scene);
        stage.show();
    }

    public static FXMLLoader getFXMLLoader(String fxml) throws IOException {
        return new FXMLLoader(App.class.getResource("fxml/" + fxml + ".fxml"));
    }

    public static Parent loadFXML(String fxml) throws IOException {
        var fxmlLoader = getFXMLLoader(fxml);
        return fxmlLoader.load();
    }

    public static void setScene(String fxml) throws IOException {
        scene = new Scene(loadFXML(fxml));
        stage.hide();
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }

    public static void newScene(String fxml) throws IOException {
        Scene scene = new Scene(loadFXML(fxml));
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    public static void main(String[] args) {
        launch();
    }

}