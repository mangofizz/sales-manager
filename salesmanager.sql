-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 30-11-2022 a las 11:49:49
-- Versión del servidor: 10.4.27-MariaDB
-- Versión de PHP: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `salesmanager`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `customer`
--

CREATE TABLE `customer` (
  `customerID` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `firstSurName` varchar(20) DEFAULT NULL,
  `secondSurName` varchar(20) DEFAULT NULL,
  `RFC` varchar(20) DEFAULT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `customer`
--

INSERT INTO `customer` (`customerID`, `name`, `firstSurName`, `secondSurName`, `RFC`, `phone`, `email`) VALUES
(1, 'Roberto', 'Perez', 'Aguirre', 'QUMA470929F37', '2288531324', 'robtopsal64@gmail.com'),
(2, 'Juan Carlos', 'Rodriguez', 'Esparza', 'GUMA470929F37', '2299323122', 'juanca.22@gmail.com'),
(3, 'Arturo', 'Castillo', 'Salas', 'CASA470929F37', '2281934234', 'sdfda@yahoo.es');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `productID` int(11) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(64) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `price` float DEFAULT NULL,
  `stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`productID`, `code`, `name`, `cost`, `price`, `stock`) VALUES
(1, '324563', 'Tacos de mango', 10, 15, 895),
(2, '643453', 'Malteada de fresa', 26, 33, 44),
(5, '789879', 'Pan molido Trimbo', 14, 17, 35),
(6, '465456', 'Chocolate Abuelo 1 pza.', 12, 16, 22),
(7, '411211', 'Rola Cola 3L', 28, 36, 12),
(8, '123123', 'Lechuga', 10, 15, 1),
(9, '745645', 'Leche deslactosada', 22, 26, 0),
(10, '135444', 'Baterias AA', 8, 12, 12);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productsold`
--

CREATE TABLE `productsold` (
  `productSoldID` int(11) NOT NULL,
  `saleID` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `productID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `productsold`
--

INSERT INTO `productsold` (`productSoldID`, `saleID`, `amount`, `productID`) VALUES
(1, 1, 1, 5),
(2, 1, 1, 9),
(3, 1, 2, 6),
(4, 3, 1, 10),
(5, 4, 1, 7),
(6, 5, 5, 1),
(7, 6, 2, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sale`
--

CREATE TABLE `sale` (
  `saleID` int(11) NOT NULL,
  `saleDate` date DEFAULT NULL,
  `saleNumber` int(11) DEFAULT NULL,
  `totalSale` float DEFAULT NULL,
  `customerID` int(11) DEFAULT NULL,
  `StoreUserID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `sale`
--

INSERT INTO `sale` (`saleID`, `saleDate`, `saleNumber`, `totalSale`, `customerID`, `StoreUserID`) VALUES
(1, '2022-11-30', 252637, 75, 1, 1),
(3, '2022-11-30', 107146, 12, 3, 1),
(4, '2022-11-30', 677901, 36, NULL, 1),
(5, '2022-11-30', 51710, 75, NULL, 1),
(6, '2022-11-30', 799731, 30, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `storeuser`
--

CREATE TABLE `storeuser` (
  `storeUserID` int(11) NOT NULL,
  `name` varchar(20) DEFAULT NULL,
  `firstSurName` varchar(20) DEFAULT NULL,
  `secondSurName` varchar(20) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` varchar(126) DEFAULT NULL,
  `type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `storeuser`
--

INSERT INTO `storeuser` (`storeUserID`, `name`, `firstSurName`, `secondSurName`, `username`, `password`, `type`) VALUES
(1, 'Alejandro', 'Salas', 'Dorantes', 'aiwass', 'wasd', 0),
(2, 'Ian', 'Rumayor', 'Romero', 'ICRR00', '01234', 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerID`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`productID`);

--
-- Indices de la tabla `productsold`
--
ALTER TABLE `productsold`
  ADD PRIMARY KEY (`productSoldID`),
  ADD KEY `saleID` (`saleID`),
  ADD KEY `productID` (`productID`);

--
-- Indices de la tabla `sale`
--
ALTER TABLE `sale`
  ADD PRIMARY KEY (`saleID`),
  ADD KEY `StoreUserID` (`StoreUserID`),
  ADD KEY `customerID` (`customerID`);

--
-- Indices de la tabla `storeuser`
--
ALTER TABLE `storeuser`
  ADD PRIMARY KEY (`storeUserID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `customer`
--
ALTER TABLE `customer`
  MODIFY `customerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `productID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `productsold`
--
ALTER TABLE `productsold`
  MODIFY `productSoldID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `sale`
--
ALTER TABLE `sale`
  MODIFY `saleID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `storeuser`
--
ALTER TABLE `storeuser`
  MODIFY `storeUserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `productsold`
--
ALTER TABLE `productsold`
  ADD CONSTRAINT `productsold_ibfk_1` FOREIGN KEY (`saleID`) REFERENCES `sale` (`saleID`),
  ADD CONSTRAINT `productsold_ibfk_2` FOREIGN KEY (`productID`) REFERENCES `product` (`productID`);

--
-- Filtros para la tabla `sale`
--
ALTER TABLE `sale`
  ADD CONSTRAINT `sale_ibfk_1` FOREIGN KEY (`StoreUserID`) REFERENCES `storeuser` (`storeUserID`),
  ADD CONSTRAINT `sale_ibfk_2` FOREIGN KEY (`customerID`) REFERENCES `customer` (`customerID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
